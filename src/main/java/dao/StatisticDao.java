package dao;

import models.Statistic;

import java.util.List;

public interface StatisticDao {

    List<Statistic> findByUserId(Long userId);
    List<Statistic> findByCarId(Long carId);
    Statistic findById(Long id);
    void add(Statistic statistic);
}
