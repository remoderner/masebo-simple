package dao;

import models.Car;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import java.util.List;

public class CarDaoImpl implements CarDao {

    private final EntityManager em;

    public CarDaoImpl(EntityManager em) {
        this.em = em;
    }

    @Override
    public List<Car> findCars(Integer userId) {
        return em.createQuery("SELECT c FROM Car c where c.userId = :userId", Car.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public void add(Car car) {
        em.getTransaction().begin();
        try {
            em.persist(car);
            em.getTransaction().commit();
        } catch (PersistenceException e) {
            em.getTransaction().rollback();
            throw e;
        }
    }
}
