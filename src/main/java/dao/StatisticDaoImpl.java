package dao;

import models.Car;
import models.Statistic;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import java.util.List;

public class StatisticDaoImpl implements StatisticDao {

    private EntityManager em;

    public StatisticDaoImpl(EntityManager em) {
        this.em = em;
    }

    @Override
    public List<Statistic> findByUserId(Long userId) {
        return em.createQuery("SELECT s FROM Statistic s where s.userId = :userId", Statistic.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public List<Statistic> findByCarId(Long carId) {
        return em.createQuery("SELECT s FROM Statistic s where s.carId = :carId", Statistic.class)
                .setParameter("carId", carId)
                .getResultList();
    }

    @Override
    public Statistic findById(Long id) {
        return em.find(Statistic.class, id);
    }

    @Override
    public void add(Statistic statistic) {
        em.getTransaction().begin();
        try {
            em.persist(statistic);
            em.getTransaction().commit();
        } catch (PersistenceException e) {
            em.getTransaction().rollback();
            throw e;
        }
    }
}
