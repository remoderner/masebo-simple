package dao;

import models.User;

public interface UserDao {

    User findById(Integer userId);
    void add(User user);
}
