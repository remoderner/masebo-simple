package dao;


import models.Car;

import java.util.List;

public interface CarDao {

    List<Car> findCars(Integer userId);
    void add(Car car);
}
