package dao;

import models.Log;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

public class LogDaoImpl implements LogDao {

    private final EntityManager em;

    public LogDaoImpl(EntityManager em) {
        this.em = em;
    }

    @Override
    public void add(Log log) {
        em.getTransaction().begin();
        try {
            em.persist(log);
            em.getTransaction().commit();
        } catch (PersistenceException e) {
            em.getTransaction().rollback();
            throw e;
        }
    }
}
