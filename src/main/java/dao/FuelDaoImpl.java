package dao;

import models.Fuel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import java.util.List;

public class FuelDaoImpl implements FuelDao {

    private final EntityManager em;

    public FuelDaoImpl(EntityManager em) {
        this.em = em;
    }

    @Override
    public List<Fuel> findFuels(Long carId) {
        return em.createQuery("SELECT f FROM Fuel f where f.carId = :carId", Fuel.class)
                .setParameter("carId", carId)
                .getResultList();
    }

    @Override
    public void add(Fuel fuel) {
        em.getTransaction().begin();
        try {
            em.persist(fuel);
            em.getTransaction().commit();
        } catch (PersistenceException e) {
            em.getTransaction().rollback();
            throw e;
        }
    }
}
