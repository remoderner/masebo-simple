package dao;

import models.Fuel;

import java.util.List;

public interface FuelDao {

    List<Fuel> findFuels(Long carId);
    void add(Fuel fuel);
}
