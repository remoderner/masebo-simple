package dao;

import models.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

public class UserDaoImpl implements UserDao {

    private final EntityManager em;

    public UserDaoImpl(EntityManager em) {
        this.em = em;
    }

    @Override
    public User findById(Integer userId) {
        return em.find(User.class, userId);
    }

    @Override
    public void add(User user) throws PersistenceException {
        em.getTransaction().begin();
        try {
            em.persist(user);
            em.getTransaction().commit();
        } catch (PersistenceException e) {
            em.getTransaction().rollback();
            throw e;
        }
    }
}
