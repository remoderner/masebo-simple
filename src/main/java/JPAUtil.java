import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {

    private static EntityManager entityManager;

    static {
        try {
            EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("MaseboPersistenceUnit");
            entityManager = entityManagerFactory.createEntityManager();
        } catch (Throwable ex) {
            ex.getStackTrace();
        }
    }

    public static EntityManager getEntityManager() {
        return entityManager;
    }
}
