package web;
import dao.LogDao;
import dao.LogDaoImpl;
import models.Log;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns = "/users/*")
public class StartupServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String userAgent = req.getHeader("User-Agent");

        resp.setContentType("text/html");
        resp.setCharacterEncoding("UTF-8");

        PrintWriter pw = resp.getWriter();
        pw.println("<!DOCTYPE html>");
        pw.println("<html><head><title>webapp</title></head>");
        pw.print("<body>");
        pw.print("<h1>Привет, ");
        pw.print(userAgent);
        pw.print("!</h1>\n");
        pw.println("</body></html>");

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("MaseboPersistenceUnit");
        EntityManager em = emf.createEntityManager();

        try {
            LogDao logDao = new LogDaoImpl(em);

            Log log = new Log("/users/*", System.currentTimeMillis());
            logDao.add(log);
        } finally {
            em.close();
            emf.close();
        }
    }
}
