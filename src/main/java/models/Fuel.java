package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "FUELS")
public class Fuel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Long carId;

    @Column
    private Float mileage;

    @Column
    private Float amountFuel;

    @Column
    private Float priceFuel;

    @Column
    private Boolean isFullTank;

    public Fuel() {
    }

    public Fuel(Long carId, Float mileage, Float amountFuel, Float priceFuel, Boolean isFullTank) {
        this.carId = carId;
        this.mileage = mileage;
        this.amountFuel = amountFuel;
        this.priceFuel = priceFuel;
        this.isFullTank = isFullTank;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCarId() {
        return carId;
    }

    public void setCarId(Long carId) {
        this.carId = carId;
    }

    public Float getMileage() {
        return mileage;
    }

    public void setMileage(Float mileage) {
        this.mileage = mileage;
    }

    public Float getAmountFuel() {
        return amountFuel;
    }

    public void setAmountFuel(Float amountFuel) {
        this.amountFuel = amountFuel;
    }

    public Float getPriceFuel() {
        return priceFuel;
    }

    public void setPriceFuel(Float priceFuel) {
        this.priceFuel = priceFuel;
    }

    public Boolean getFullTank() {
        return isFullTank;
    }

    public void setFullTank(Boolean fullTank) {
        isFullTank = fullTank;
    }
}
