package models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "CARS")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Long userId;

    @Column
    private String name;

    @Column
    private Float initialMileage;

    @ManyToOne(cascade = CascadeType.ALL)
    private User user;

    public Car() {
    }

    public Car(Long userId, String name, Float initialMileage) {
        this.userId = userId;
        this.name = name;
        this.initialMileage = initialMileage;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getInitialMileage() {
        return initialMileage;
    }

    public void setInitialMileage(Float initialMileage) {
        this.initialMileage = initialMileage;
    }
}
