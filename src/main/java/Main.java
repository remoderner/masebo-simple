import dao.UserDao;
import dao.UserDaoImpl;
import models.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("MaseboPersistenceUnit");
        EntityManager em = emf.createEntityManager();

        try {
            UserDao userDao = new UserDaoImpl(em);

            User user = new User(1L, "test_login", "pass");
            userDao.add(user);
        } finally {
            em.close();
            emf.close();
        }
    }
}
